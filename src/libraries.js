import Vue from "vue";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import locale from "element-ui/lib/locale/lang/en";
import Sortable from "vue-sortable";
import VueDraggable from "vue-draggable";

Vue.use(ElementUI, { locale });
Vue.use(Sortable);
Vue.use(VueDraggable);
