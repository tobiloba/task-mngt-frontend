import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "AssignTask",
    component: () =>
    import(/* webpackChunkName: "about" */ "../views/AssignTask.vue")
 },
  {
    path: "/manageTask",
    name: "AssignTask",

    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AssignTask.vue")
  },
  // {
  //   path: "/addNewUser",
  //   name: "AddNewUser",

  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/AddNewUser.vue")
  // }
];

const router = new VueRouter({
  routes
});

export default router;
